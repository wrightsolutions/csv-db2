#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from __future__ import print_function
import re
from sys import argv,exit
from os import path as ospath
from os import stat
import csv

from string import printable
set_printable = set(printable)
ASTER=chr(42)
COMMA=chr(44)

QUOTING_OUTPUT = csv.QUOTE_MINIMAL
#QUOTING_OUTPUT = csv.QUOTE_ALL
"""
Above enable whichever of csv.QUOTE_MINIMAL or csv.QUOTE_ALL 
you prefer. csv.QUOTE_ALL is going to force quoting regardless
of the need and is helpful when the data input target
is expecting every field to be quoted.
"""

""" If this program is developed further then we might want to
consider minor before or after fixes for common formatting issues
RE_COMMA_ENDING_LOOSE = re.compile(r',\s*\Z')
RE_COMMA_ENDING = re.compile(r',\Z')
"""


def isfile_and_positive_size(target_path_given):
	target_path = target_path_given.strip()
	if not ospath.isfile(target_path):
		return False
	if not stat(target_path).st_size:
		return False
	return True


def lines_from_shortform(csvfile_given=None):
	filepath_expanded = ospath.expanduser(csvfile_given)
	with open(filepath_expanded,'r') as csvfile:
		reader = csv.reader(csvfile,delimiter=chr(44))
		#headrow = reader1.next()
		#reader = csv.DictReader(csvfile)
		for idx,dline in enumerate(reader):
			if len(dline) < 1:
				break
			dname = dline[0]
			tname = dline[1]
			shortform = dline[2]
			grantee1 = dline[3]
			GTEMPLATE = "GRANT {0} ON `dname`.{1} TO {2};"
			if tname.strip().startswith(ASTER):
				table = tname.strip()
			else:
				table = "`{0}`".format(tname.strip())
			grantee = "'{0}'@'%'".format(grantee1)
			grants = ''
			delim = ''
			if 's' in shortform:
				grants = 'SELECT'
				delim = COMMA
			if 'i' in shortform:
				grants += delim+'INSERT'
				delim = COMMA
			if 'u' in shortform:
				grants += delim+'UPDATE'
				delim = COMMA
			if 'd' in shortform:
				grants += delim+'DELETE'
				delim = COMMA
			if 't' in shortform:
				grants += delim+'TRIGGER'
				delim = COMMA
			print(GTEMPLATE.format(grants,table,grantee))
	return


if __name__ == "__main__":
	if (len(argv) < 2):
		exit(101)
	lines = lines_from_shortform('/tmp/grants.csv')
	exit

