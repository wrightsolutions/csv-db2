#!/usr/bin/env python
#   Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

# For the given csv file, take the content of two source fields 
# and populate the target field with whichever of the two source fields is nonblank
#
# Target field name is the first argument
# Source field 1 name is the second argument
# Source field 2 name is the third argument
# Mode is the fourth argument and is either 'copy' or 'move' defaulting to 'copy'
#
# Priority is given to Source field 1, so if both source field 1 and source field 2
# are non blank, then target will take the contents of source field 1

# Can be used to select whichever of two fields is nonblank
# as described next.
# Suppose you have a two field output from a home ownership system,
# but for some reason where there is just one owner the owner sometimes (but not always)
# appears in the second field with the first field blank
#
# So the first 9 lines of the two field csv file might look like this with field headings owner1 and owner2
# "Euler","Fermat"
# "","Fermat"
# "Russell","Whitehead"
# "Hardy","Ramanujan"
# "Boole",""
# "","Gauss"
# "Lucas",""
# "Lucas","Lehmer"
# "",""


# To fix the above so that where only one of the fields is populated, then just the first field
# is populated you would use the following 5 argument form:
# filename owner1 owner1 owner2 move
# Listing the argument names we are saying for filename csv file operate using target,source1,source2,mode
#
# And requesting that where only one of 'owner1' and 'owner2' is populated,
# that we move that source to be in target 'owner1'
#
# The name 'keep' in the filename is intended to reflect the fact that when used in the scenario
# above, we have rearranged but kept all the data rather than losing some.

# If you select mode of 'copy' then you are knowingly asking for what might be considered duplication
# Sometimes this is the better choice of outcomes depending on what you are working with.

# Results of the above when in 'move' mode would be the following EIGHT rows:
# Euler,Fermat
# Fermat,
# Russell,Whitehead
# Hardy,Ramanujan
# Boole,
# Gauss,
# Lucas,
# Lucas,Lehmer

# Results of the above when QUOTE_ALL is in effect and 'copy' mode would be the following EIGHT rows:
# "Euler","Fermat"
# "Fermat","Fermat"
# "Russell","Whitehead"
# "Hardy","Ramanujan"
# "Boole",""
# "Gauss","Gauss"
# "Lucas",""
# "Lucas","Lehmer"

from __future__ import print_function
import re
from sys import argv,exit
from os import path as ospath
from os import stat
import csv

from string import printable
set_printable = set(printable)

QUOTING_OUTPUT = csv.QUOTE_MINIMAL
#QUOTING_OUTPUT = csv.QUOTE_ALL
"""
Above enable whichever of csv.QUOTE_MINIMAL or csv.QUOTE_ALL 
you prefer. csv.QUOTE_ALL is going to force quoting regardless
of the need and is helpful when the data input target
is expecting every field to be quoted.
"""

""" If this program is developed further then we might want to
consider minor before or after fixes for common formatting issues
RE_COMMA_ENDING_LOOSE = re.compile(r',\s*\Z')
RE_COMMA_ENDING = re.compile(r',\Z')
"""


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not stat(target_path).st_size:
        return False
    return True


def two_to_one(filepath,target,source1,source2,mode,outfile=None,fieldnames=None):
    """ This function has an optional final argument which allows this function
    to be used either as a read only pass through the file, or (when outfile
    is given a file already open for writing) as a writer that makes changes

    Use of both types of call helps with a two pass setup.

    This is particularly useful when a file might have no changes and the
    reporting of this might have not completed because of an output file initialisation
    issue. When there are no changes the second pass would not be required and the
    first pass would complete unhindered.

    We use a variable actioned which will have a positive value when some
    changes are actioned (or planned in preview)

    When no outfile is supplied and we are effectively just previewing a better
    variable name might be 'tagged'.
    """

    actioned = -3

    filepath_expanded = ospath.expanduser(filepath)

    if not isfile_and_positive_size(filepath):
        return actioned

    actioned = -2

    fieldnames_list = []
    if fieldnames is not None:
        try:
            if len(fieldnames_list) > 0:
                fieldnames_list = fieldnames
            """ fieldnames has something and its length is positive
            so assign it to fieldnames_list
            """
        except:
            print("fieldnames_list could not be initialised!")
            raise

    outfile_writer = None

    if outfile is None:
        """ Either we are operating in a two pass scenario or the user just
        wants to analyse things or preview. No csv writer object will be required
        """
        actioned = -1
    elif len(fieldnames_list) > 0:
        """ We will try and make any relevant changes to the supplied output file
        so will need a csv writer helper with which to work. That is initialised next.
        """
        try:
            outfile_writer = csv.DictWriter(outfile, fieldnames=fieldnames_list, delimiter=chr(44),
                                            quotechar=chr(34), quoting=QUOTING_OUTPUT)
            actioned = -1
        except:
            raise
    else:
        with open(filepath_expanded,'r') as csvfile1:
            reader1 = csv.reader(csvfile1,delimiter=chr(44))
            headrow = reader1.next()
            fieldnames_list = []
            for fieldname in headrow:
                fieldnames_list.append(fieldname.strip('\'"'))
        try:
            second = fieldnames_list[1]
        except:
            raise RuntimeError("Outfile set but have no way of determining fieldnames!")
        try:
            outfile_writer = csv.DictWriter(outfile, fieldnames=fieldnames_list, delimiter=chr(44),
                                            quotechar=chr(34), quoting=QUOTING_OUTPUT)
            actioned = -1
        except:
            raise

    if outfile is not None:
        print("Writing of changes will be actioned in '{0}' mode".format(mode))

    with open(filepath_expanded,'r') as csvfile:
        # print("Opened csvfile {0}".format(filepath))
        actioned = 0
        if len(fieldnames_list) > 0 and fieldnames is not None:
            """ If we were given fieldnames as a parameter rather than
            deriving them, then use the contents of fieldnames_list 
            when initilialising
            """
            reader = csv.DictReader(csvfile,fieldnames_list)
        else:
            reader = csv.DictReader(csvfile)
        for idx,dline in enumerate(reader):
            if idx == 0 and len(fieldnames_list) < 1:
                if target in dline and source1 in dline and source2 in dline:
                    """ Have what appear to be a header entries in first row """
                    #print("HEADER: {}".format(dline))
                    pass
                else:
                    print("Warning: We have no fieldnames data and do not think",end='')
                    print(" first row is a header row!")
                    break
            print(dline)
            targval = dline[target]
            source1value = dline[source1]
            source2value = dline[source2]
            print("target,source1,source2 appear as {0},{1},{2}".format(targval,source1value,source2value))
            actioned_fieldname = None
            actioned_value_original = None
            sourced_fieldname = None
            if len(source1value) > 0 and len(source2value) > 0:
                print("Line {0} has both source fields populated".format(idx))
                if target == source1value:
                    # do nothing
                    pass
                elif target == source2value:
                    # do nothing
                    pass
                else:
                    """ Here target is not self sourcing meaning neither
                    of source1value or source2value is the target
                    """
                    sourced_fieldname = source1
            elif len(source1value) > 0:
                print(source1value)
                if target == source1value:
                    # do nothing
                    pass
                else:
                    sourced_fieldname = source1
            elif len(source2value) > 0:
                print(source2value)
                if target == source2value:
                    # do nothing
                    pass
                else:
                    sourced_fieldname = source2
            else:
                print("Line {0} has neither source field populated".format(idx))

            if sourced_fieldname is None:
                continue

            """ Having reached this point we have an action to perform """
                
            if set(dline[sourced_fieldname]).issubset(set_printable):
                pass
            else:
                print("Warning: Line {0} sourced item '{1}' has non-printable characters") 


            preview_or_intention = 'will be'
            if outfile_writer is None:
                """ Change what we are reporting in print to be less certain
                as only previewing
                """
                preview_or_intention = '... would be ...'

            actioned_value_original = targval
            try:
                dline[target] = dline[sourced_fieldname]
                actioned_value = dline[sourced_fieldname]
                actioned_fieldname = sourced_fieldname
            except:
                actioned_value = None

            if actioned_fieldname is None:
                # We have done nothing so nothing to report upon
                pass
            elif actioned_value_original == actioned_value:
                # We think we should have done something but cannot say what change was
                pass
            else:
                # Change has been made report in form oldvalue<-newvalue
                print("Line {0} {1}".format(idx,preview_or_intention),end='')
                if len(actioned_value_original) < 1:
                    print(' changed ""<-{}'.format(actioned_value))
                else:
                    print(" changed {0}<-{1}".format(actioned_value_original,actioned_value))

                if sourced_fieldname is not None and mode == 'move':
                    """ We have actioned something and we are not operating in 'copy' mode
                    so we need to blank out the source that caused us to populate target
                    with the change
                    """
                    print("Line {0} will have field '{1}' blanked out.".format(idx,sourced_fieldname))
                    dline[sourced_fieldname] = ''

            actioned += 1

            if outfile_writer is None:
                continue

            if idx == 0:
                # outfile_writer.writeheader()
                pass
            outfile_writer.writerow(dline)
            """ actioned += 1
            We will not increment actioned late in the loop as even when
            just previewing, we want the count to be incremented 
            """

    if outfile is not None and actioned > 0:
        print("Writing of changes actioned in '{0}' mode is now completed.".format(mode))

    return actioned


if __name__ == "__main__":
    if (len(argv) < 4):
        exit(101)
    mode = 'copy'
    if len(argv) > 4 and argv[5] in ['copy','move']:
        mode = argv[5]
    actioned_to_one = two_to_one(argv[1],argv[2],argv[3],argv[4],mode)
    if actioned_to_one == 0:
        print("Warning: no changes actioned")
    elif actioned_to_one < 0:
        print("Warning: input file issue or other malfunction")
        exit(102)
    else:
        filepath_outfile = None
        filepath_expanded = ospath.expanduser(argv[1])
        try:
            filename = ospath.basename(filepath_expanded)
            filename_without_extension = ospath.splitext(filename)[0]
            filepath_outfile = "/tmp/{0}.changed.csv".format(filename_without_extension)
        except:
            raise
        with open(filepath_outfile,'w') as csvfile:
            actioned_to_one = two_to_one(argv[1],argv[2],argv[3],argv[4],
                                         mode,csvfile)
    exit
