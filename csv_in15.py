#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (c) 2018 Gary Wright http://www.wrightsolutions.co.uk/contact
# Distributed under the BSD '3 clause' software license, see the accompanying
# file COPYING or https://opensource.org/licenses/BSD-3-Clause

from sys import argv,exit
from os import path as ospath
from os import stat
import csv

# python ./sql_in15.py | awk '{print "SELECT u_email FROM users WHERE u_old_id "$1";"}'

INFILE_PATHED = '/tmp/id.csv'


def isfile_and_positive_size(target_path_given):
	target_path = target_path_given.strip()
	if not ospath.isfile(target_path):
		return False
	if not stat(target_path).st_size:
		return False
	return True


if __name__ == "__main__":


	if not isfile_and_positive_size(INFILE_PATHED):
		exit(111)

	with open(INFILE_PATHED, 'r') as file:
		reader = csv.reader(file)
		in_list = []
		for row in reader:
			in_list.append(row[0])
			if len(in_list) > 9:
				in_list = [ "'{0}'".format(id) for id in in_list ]
				in_string = "IN({0})".format(','.join(in_list))
				print(in_string)
				in_list = []

	if len(in_list) > 0:
		in_list = [ "'{0}'".format(id) for id in in_list ]
		in_string = "IN({0})".format(','.join(in_list))
		print(in_string)



