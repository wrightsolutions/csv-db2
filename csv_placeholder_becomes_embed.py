#!/usr/bin/env python
#   Copyright 2015 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0

# For the given csv file, where field itself contains the comma placeholder COMAMAC0
# then replace that placeholder with a real comma

# This is the second part of a two stage process. This script is reversing the
# effects of the first script csv_embed_placeholder.py

# Rationale for placeholders generally:
# With the placeholder instead of embedded comma, working with stream editors
# or awk becomes more straightforward

# Header row note: We require a header row or some other way for the headers
# to be supplied. If you encounter RuntimeError: Header confusion
# then you need to add a header to the top of the file

# If you do not require any of the previewing features and are sure
# that COMAMAC0 does not clash with any of your data, then you might want to just sed:
#  sed 's/COMAMAC0/,/g' placeholdered.csv > becomes_embed.csv

from __future__ import print_function
import re
from sys import argv,exit
from os import path as ospath
from os import stat
import csv

from string import printable
set_printable = set(printable)

PLACEHOLDER_COMMA='COMAMAC0'

#READER_QUOTECHAR=chr(34)
READER_QUOTECHAR=None
WRITER_QUOTECHAR=chr(34)


#QUOTING_OUTPUT = csv.QUOTE_MINIMAL
QUOTING_OUTPUT = csv.QUOTE_ALL
"""
Above enable whichever of csv.QUOTE_MINIMAL or csv.QUOTE_ALL 
you prefer. csv.QUOTE_ALL is going to force quoting regardless
of the need and is helpful when the data input target
is expecting every field to be quoted.
csv.QUOTE_NONE may result in csv.Error: need to escape
If csv.QUOTE_ALL seems too much then try csv.QUOTE_MINIMAL
"""

RE_COMMA_ANYWHERE = re.compile(r',')
RE_PLACEHOLDER_ANYWHERE = re.compile(PLACEHOLDER_COMMA)


def isfile_and_positive_size(target_path_given):
    target_path = target_path_given.strip()
    if not ospath.isfile(target_path):
        return False
    if not stat(target_path).st_size:
        return False
    return True


def embed_placeholder(filepath,embedded=None,placeholder=None,outfile=None,fieldnames=None):
    """ This function has an optional final argument which allows this function
    to be used either as a read only pass through the file, or (when outfile
    is given a file already open for writing) as a writer that makes changes

    Use of both types of call helps with a two pass setup.

    This is particularly useful when a file might have no changes and the
    reporting of this might have not completed because of an output file initialisation
    issue. When there are no changes the second pass would not be required and the
    first pass would complete unhindered.

    We use a variable actioned which will have a positive value when some
    changes are actioned (or planned in preview)

    When no outfile is supplied and we are effectively just previewing a better
    variable name might be 'tagged'.
    """

    if embedded == chr(44) or embedded is None:
        if placeholder is None:
            placeholder = PLACEHOLDER_COMMA

    if embedded is None:
        print("Later in processing, the placeholder {0} will be ".format(placeholder),end='')
        embedded = chr(44)
        print("replaced with embed={0}".format(embedded))

    actioned = -3

    filepath_expanded = ospath.expanduser(filepath)

    if not isfile_and_positive_size(filepath):
        return actioned

    actioned = -2

    fieldnames_list = []
    if fieldnames is not None:
        try:
            if len(fieldnames_list) > 0:
                fieldnames_list = fieldnames
            """ fieldnames has something and its length is positive
            so assign it to fieldnames_list
            """
        except:
            print("fieldnames_list could not be initialised!")
            raise

    outfile_writer = None

    header_as_list = []
    if outfile is None:
        """ Either we are operating in a two pass scenario or the user just
        wants to analyse things or preview. No csv writer object will be required
        """
        actioned = -1
    elif len(fieldnames_list) > 0:
        """ We will try and make any relevant changes to the supplied output file
        so will need a csv writer helper with which to work. That is initialised next.
        """
        try:
            outfile_writer = csv.DictWriter(outfile, fieldnames=fieldnames_list,
                                            delimiter=chr(44), quotechar=WRITER_QUOTECHAR,
                                            quoting=QUOTING_OUTPUT, lineterminator='\n')
            actioned = -1
        except:
            raise
    else:
        with open(filepath_expanded,'r') as csvfile1:
            reader1 = csv.reader(csvfile1,delimiter=chr(44),quotechar=READER_QUOTECHAR)
            header_as_list = reader1.next()
            fieldnames_list = []
            for fieldname in header_as_list:
                fieldnames_list.append(fieldname.strip('\'"'))
        try:
            second = fieldnames_list[1]
        except:
            raise RuntimeError("Outfile set but have no way of determining fieldnames!")
        try:
            if len(header_as_list) > 0:
                outfile.write("{0}\n".format(chr(44).join(header_as_list)))
                outfile_writer = csv.DictWriter(outfile, fieldnames=fieldnames_list,
                                            delimiter=chr(44), quotechar=WRITER_QUOTECHAR,
                                            quoting=QUOTING_OUTPUT, lineterminator='\n')
            actioned = -1
        except:
            raise

    if placeholder is None or embedded is None:
        return actioned
    elif placeholder == embedded:
        return actioned

    print("Input file {0} will be processed using ".format(filepath),end='')
    print("fieldnames_list having length of {0}".format(len(fieldnames_list)))

    with open(filepath_expanded,'r') as csvfile:
        # print("Opened csvfile {0}".format(filepath))
        actioned = 0
        if len(fieldnames_list) > 0 and fieldnames is not None:
            """ If we were given fieldnames as a parameter rather than
            deriving them, then use the contents of fieldnames_list 
            when initilialising
            """
            reader = csv.DictReader(csvfile,fieldnames_list)
        else:
            reader = csv.DictReader(csvfile)
        for idx,dline in enumerate(reader):
            if idx == 0 and len(fieldnames_list) < 1:
                # Any first line / header processing in here
                pass
            print(dline)

            preview_or_intention = 'will be'
            if outfile_writer is None:
                """ Change what we are reporting in print to be less certain
                as only previewing
                """
                preview_or_intention = '... would be ...'

            #fields_requiring_placeholder_list = [k for k, v in dline.items() if chr(44) in v]
            fields_requiring_placeholder_dict = {k: v for k, v in dline.items() if v and placeholder in v}
            actioned_flag = False
            printable_flag = True
            if outfile_writer is None:
                print("Previewing for line {0} predicts this line requires ".format(idx),end='')
                print("{0} fields be changed".format(len(fields_requiring_placeholder_dict)))

            for fieldname,fieldvalue in fields_requiring_placeholder_dict.items():

                if not set(fieldvalue).issubset(set_printable):
                    printable_flag = False
                elif PLACEHOLDER_COMMA in fieldname:
                    raise RuntimeError("Header confusion or other issue around fieldname={0}".format(fieldname))
                else:
                    pass
                re_after = RE_PLACEHOLDER_ANYWHERE.subn(chr(44),fieldvalue)
                if re_after[1] > 0:
                    # Change has been made
                    print("Line {0} {1} ".format(idx,preview_or_intention),end='')
                    print("changed field name {0} to include".format(fieldname),end='')
                    #print(" copies of placeholder={0}".format(placeholder))
                    print(" copies of embedded={0}".format(embedded))
                    dline[fieldname] = re_after[0]
                    actioned_flag = True

            if actioned_flag is True:
                actioned += 1

            if outfile_writer is None:
                if printable_flag is False:
                    print("Warning: Line {0} sourced item '{1}' has non-printable characters") 
                continue

            if idx == 0:
                # outfile_writer.writeheader()
                pass
            outfile_writer.writerow(dline)
            """ actioned += 1
            We will not increment actioned late in the loop as even when
            just previewing, we want the count to be incremented 
            """

    if outfile is not None and actioned > 0:
        print("Writing of {0} actioned changes now completed.".format(actioned))

    return actioned


if __name__ == "__main__":
    if (len(argv) < 2):
        exit(101)
    embedded = None
    placeholder = None
    if (len(argv) > 1):
        filepath = argv[1]
        if (len(argv) > 2):
            embedded = argv[2]
            if (len(argv) > 3):
                placeholder = argv[3]

    placeholder_count = embed_placeholder(filepath,embedded,placeholder,None,None)
    if placeholder_count == 0:
        print("Warning: no changes actioned")
    elif placeholder_count < 0:
        print("Warning: input file issue or other malfunction")
        exit(102)
    else:
        filepath_outfile = None
        filepath_expanded = ospath.expanduser(argv[1])
        try:
            filename = ospath.basename(filepath_expanded)
            filename_without_extension = ospath.splitext(filename)[0]
            filepath_outfile = "/tmp/{0}.changed.csv".format(filename_without_extension)
        except:
            raise
        with open(filepath_outfile,'w') as csvfile:
            placeholder_count = embed_placeholder(filepath,embedded,placeholder,csvfile,None)
    exit
